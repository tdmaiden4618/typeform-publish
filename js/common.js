$(function(){
    // mobile menu bar
    $('.menu_btn').click(function () {
        $('nav').fadeIn();
        $('.gnb_list').css('left', '0');
    });
	$('.close_btn').click(function () {
        $('.gnb_list').css('left', '-9999px');
		$('nav').fadeOut();
	})

	// fixed header
	var $header_height = $('header').offset().top + $('header').outerHeight(true);

	$(window).scroll(function(){
		if ($(this).scrollTop() > $header_height){
			$('header').addClass('fix_header');
		}else {
			$('header').removeClass('fix_header');
		}
	});
});

// apply modal

var $totalPage = $(".apply_item").length;
var $fullsection = $(".apply_item");
var $modal = $('.apply_inner');

// page scroll event
autoScroll();

function autoScroll(){
    // up down btn
	
	$(".next_btn").on("click", function(){
        var itemHeight = $('.apply_item').outerHeight();
		var thisPosition = $modal.scrollTop() + 10;

		if(thisPosition < itemHeight){
            autoScroll('down', $fullsection.eq(0))
        }else{
            var result = parseInt(thisPosition / itemHeight);
            autoScroll('down',  $fullsection.eq(result))
        }
    })
	$(".prev_btn").on("click", function(){
        var itemHeight = $('.apply_item').outerHeight();
        var thisPosition = $modal.scrollTop() + 10;

		if(thisPosition < itemHeight){
            autoScroll('up', $fullsection.eq(0))
        }else{
            var result = parseInt(thisPosition / itemHeight);
            autoScroll('up',  $fullsection.eq(result))
        }
    })
	
    bindWheel(true);
    function bindWheel(useBoolean) {
        // wheel
        if(useBoolean) {
            $fullsection.on('wheel DOMMouseScroll', function(e) {
                e.preventDefault();
                var evnetListen = e.originalEvent;
                if (evnetListen.deltaY < 0) {
                    autoScroll('up', this);
                }else{
                    autoScroll('down', this);
                }
            });
        } else {
            $fullsection.off('wheel DOMMouseScroll');
        }
    }
    function autoScroll(direction, obj){
        bindWheel();
        var targetTop = 0;
        var itemHeight = $('.apply_item').outerHeight();
        var pageNum = parseFloat($(obj).attr("pageNum"));

        if(direction == 'down'){
            if(pageNum - 1 < $totalPage){
                targetTop = pageNum * itemHeight;
            }else{
                targetTop = $totalPage * itemHeight;
            }
        } else {
            if(pageNum != 2){
                targetTop = (pageNum - 2) * itemHeight;
            }else{
                targetTop = 0;
            }
        }
        $modal.filter(":not(:animated)").animate({'scrollTop': targetTop}, 500);
        bindWheel(true);
    }
}

